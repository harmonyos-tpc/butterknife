package butterknife;

import butterknife.internal.ListenerClass;
import butterknife.internal.ListenerMethod;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Bind a method to a { TextObserver TextObserver} on the view for each ID specified.
 * <pre><code>
 * {@literal @}onTextUpdated(ResourceTable.Id_example) void onTextUpdated(CharSequence text) {
 *   //Text change action
 * }
 * </code></pre>
 * Any number of parameters from { TextObserver#onTextUpdated(String, int, int, int)
 * onTextUpdated} may be used on the method.
 * <p>
 * To bind to methods other than {@code onTextUpdated}, specify a different {@code callback}.
 * <pre><code>
 * {@literal @}onTextUpdated(value = ResourceTable.Id_example, callback = BEFORE_TEXT_CHANGED)
 * void onBeforeTextChanged(CharSequence text) {
 *   //Before Text change action
 * }
 * </code></pre>
 * <p>
 * TextObserver
 */
@Target(METHOD)
@Retention(RUNTIME)
@ListenerClass(
        targetType = "ohos.agp.components.Text",
        setter = "addTextObserver",
        type = "ohos.agp.components.Text.TextObserver",
        method = @ListenerMethod(
                name = "onTextUpdated",
                parameters = {
                        "java.lang.String",
                        "int",
                        "int",
                        "int"
                }
        )
)


public @interface OnTextChanged {
    /**
     * Component IDs to which the method will be bound.
     */
    int[] value() default { /* View.NO_ID */};
}

