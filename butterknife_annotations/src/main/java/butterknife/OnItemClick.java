package butterknife;

import butterknife.internal.ListenerClass;
import butterknife.internal.ListenerMethod;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Bind a method to an { ItemClickedListener ItemClickedListener} on the component for each ID
 * specified.
 * <pre><code>
 * {@literal @}OnItemClick(ResourceTable.Id_example_list) void onItemClicked(ListContainer parent, Component component, int position, long id) {
 *   //Process click on position
 * }
 * </code></pre>
 * Any number of parameters from { ItemClickedListener#onItemClicked(ohos.agp.components.ListContainer,
 * ohos.agp.components.Component, int, long) onItemClicked} may be used on the method.
 * <p>
 * ItemClickedListener
 */
@Target(METHOD)
@Retention(RUNTIME)
@ListenerClass(
        targetType = "ohos.agp.components.ListContainer",
        setter = "setItemClickedListener",
        type = "ohos.agp.components.ListContainer.ItemClickedListener",
        method = @ListenerMethod(
                name = "onItemClicked",
                parameters = {
                        "ohos.agp.components.ListContainer",
                        "ohos.agp.components.Component",
                        "int",
                        "long"
                }
        )
)
public @interface OnItemClick {
    /**
     * Component IDs to which the method will be bound.
     */
    int[] value() default { /* View.NO_ID */};
}
