package butterknife;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Bind a field to the specified font resource ID.
 *   style = 0 (Typeface.REGULAR)
 *   style = 1 (Typeface.BOLD)
 *   style = 2 (Typeface.ITALIC)
 *   style = 3 (Typeface.BOLD_ITALIC)
 * <pre><code>
 * {@literal @}@BindFont(value = ResourceTable.String_aargh_font, style = 2)
 * </code></pre>
 */
@Target(FIELD)
@Retention(RUNTIME)
public @interface BindFont {
    /**
     * Font resource ID to which the field will be bound.
     */
    int value();

    /*Typeface.REGULAR = 0,
    Typeface.BOLD = 1,
    Typeface.ITALIC = 2,
    Typeface.BOLD_ITALIC = 3*/

    @TypefaceStyle int style() default 0/*Font.REGULAR*/;

    @interface TypefaceStyle {
    }
}
