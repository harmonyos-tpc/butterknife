package butterknife;

import butterknife.internal.ListenerClass;
import butterknife.internal.ListenerMethod;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Bind a method to an { CheckedStateChangedListener CheckedStateChangedListener} on the component for
 * each ID specified.
 * <pre><code>
 * {@literal @}OnCheckedChanged(ResourceTable.Id_example) void onChecked(boolean checked) {
 *   // is checked action
 * }
 * </code></pre>
 * Any number of parameters from
 * { CheckedStateChangedListener#onCheckedChanged(RadioContainer container, int checkedId)
 * onCheckedChanged} may be used on the method.
 * <p>
 * CheckedStateChangedListener
 */
@Target(METHOD)
@Retention(RUNTIME)
@ListenerClass(
        targetType = "ohos.agp.components.RadioContainer",
        setter = "setMarkChangedListener",
        type = "ohos.agp.components.RadioContainer.CheckedStateChangedListener",
        method = @ListenerMethod(
                name = "onCheckedChanged",
                parameters = {
                        "ohos.agp.components.RadioContainer",
                        "int"
                }
        )
)
public @interface OnCheckedChanged {
    /**
     * Component IDs to which the method will be bound.
     */
    int[] value() default { /* View.NO_ID */};
}
