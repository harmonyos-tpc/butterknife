package butterknife;

import butterknife.internal.ListenerClass;
import butterknife.internal.ListenerMethod;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Bind a method to an { TouchEventListener TouchEventListener} on the view for each ID specified.
 * <pre><code>
 * {@literal @}OnTouch(ResourceTable.Id_example) boolean onTouchEvent() {
 *   //Touch action
 *   return false;
 * }
 * </code></pre>
 * Any number of parameters from
 * { TouchEventListener#onTouchEvent(Component, TouchEvent) onTouch} may be used
 * on the method.
 * <p>
 * If the return type of the method is {@code void}, true will be returned from the listener.
 * <p>
 * TouchEventListener
 */
@Target(METHOD)
@Retention(RUNTIME)
@ListenerClass(
        targetType = "ohos.agp.components.Component",
        setter = "setTouchEventListener",
        type = "ohos.agp.components.Component.TouchEventListener",
        method = @ListenerMethod(
                name = "onTouchEvent",
                parameters = {
                        "ohos.agp.components.Component",
                        "ohos.multimodalinput.event.TouchEvent"
                },
                returnType = "boolean",
                defaultReturn = "true"
        )
)
public @interface OnTouch {
    /**
     * Component IDs to which the method will be bound.
     */
    int[] value() default { /* View.NO_ID */};
}
