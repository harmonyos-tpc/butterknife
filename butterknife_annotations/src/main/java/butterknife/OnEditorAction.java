package butterknife;

import butterknife.internal.ListenerClass;
import butterknife.internal.ListenerMethod;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Bind a method to an { OnEditorActionListener OnEditorActionListener} on the component for each
 * ID specified.
 * <pre><code>
 * {@literal @}OnEditorAction(ResourceTable.Id_example) boolean onTextEditorAction(int action) {
 *    //Key press listen action
 *   return true;
 * }
 * </code></pre>
 * Any number of parameters from
 * { OnEditorActionListener#onTextEditorAction(int action)
 * onTextEditorAction} may be used on the method.
 * <p>
 * If the return type of the method is {@code void}, true will be returned from the listener.
 * <p>
 * OnEditorActionListener
 */
@Target(METHOD)
@Retention(RUNTIME)
@ListenerClass(
        targetType = "ohos.agp.components.Text",
        setter = "setEditorActionListener",
        type = "ohos.agp.components.Text.EditorActionListener",
        method = @ListenerMethod(
                name = "onTextEditorAction",
                parameters = {
                        "int"
                },
                returnType = "boolean",
                defaultReturn = "true"
        )
)
public @interface OnEditorAction {
    /**
     * Component IDs to which the method will be bound.
     */
    int[] value() default { /* View.NO_ID */};
}
