package butterknife;

import butterknife.internal.ListenerClass;
import butterknife.internal.ListenerMethod;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Bind a method to an { ItemLongClickedListener ItemLongClickedListener} on the component for each
 * ID specified.
 * <pre><code>
 * {@literal @}OnItemLongClick(ResourceTable.Id_example_list) boolean onItemLongClicked(ListContainer parent,
 *                           Component component,
 *                           int position,
 *                           long id) {
 *   // Perform long click action;
 *   return true;
 * }
 * </code></pre>
 * Any number of parameters from
 * { ItemLongClickedListener#onItemLongClick(ListContainer, ohos.agp.components.Component,
 * int, long) onItemLongClick} may be used on the method.
 * <p>
 * If the return type of the method is {@code void}, true will be returned from the listener.
 * <p>
 * ItemLongClickedListener
 */
@Target(METHOD)
@Retention(RUNTIME)
@ListenerClass(
        targetType = "ohos.agp.components.ListContainer",
        setter = "setItemLongClickedListener",
        type = "ohos.agp.components.ListContainer.ItemLongClickedListener",
        method = @ListenerMethod(
                name = "onItemLongClicked",
                parameters = {
                        "ohos.agp.components.ListContainer",
                        "ohos.agp.components.Component",
                        "int",
                        "long"
                },
                returnType = "boolean",
                defaultReturn = "false"
        )
)
public @interface OnItemLongClick {
    /**
     * View IDs to which the method will be bound.
     */
    int[] value() default { /* View.NO_ID */};
}
