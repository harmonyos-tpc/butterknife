package butterknife;

import butterknife.internal.ListenerClass;
import butterknife.internal.ListenerMethod;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Bind a method to an {@code PageChangedListener} on the component for each ID specified.
 * <pre><code>
 * {@literal @}OnPageChange(ResourceTable.Id_example_pager) void onPageChosen(int itemPos) {
 *   //Page Change action
 * }
 * </code></pre>
 * Any number of parameters from {@code onPageChosen} may be used on the method.
 * <p>
 * To bind to methods other than {@code onPageChosen}, specify a different {@code callback}.
 * <pre><code>
 * {@literal @}onPageChosen(value = ResourceTable.Id_example_pager, callback = PAGE_SCROLL_STATE_CHANGED)
 * void onPageSlideStateChanged(int state) {
 *   //Page state Change action
 * }
 * </code></pre>
 */
@Target(METHOD)
@Retention(RUNTIME)
@ListenerClass(
        targetType = "ohos.agp.components.PageSlider",
        setter = "addPageChangedListener",
        remover = "removePageChangedListener",
        type = "ohos.agp.components.PageSlider.PageChangedListener",
        callbacks = OnPageChange.Callback.class
)
public @interface OnPageChange {
    /**
     * View IDs to which the method will be bound.
     */
    int[] value() default { /* View.NO_ID */};

    /**
     * Listener callback to which the method will be bound.
     */
    Callback callback() default Callback.PAGE_SELECTED;

    enum Callback {
        /**
         * {@code onPageChosen(int)}
         */
        @ListenerMethod(
                name = "onPageChosen",
                parameters = "int"
        )
        PAGE_SELECTED,

        /**
         * {@code onPageSliding(int, float, int)}
         */
        @ListenerMethod(
                name = "onPageSliding",
                parameters = {
                        "int",
                        "float",
                        "int"
                }
        )
        PAGE_SCROLLED,

        /**
         * {@code onPageSlideStateChanged(int)}
         */
        @ListenerMethod(
                name = "onPageSlideStateChanged",
                parameters = "int"
        )
        PAGE_SCROLL_STATE_CHANGED,
    }
}
