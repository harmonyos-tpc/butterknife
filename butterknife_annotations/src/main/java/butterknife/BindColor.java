package butterknife;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Bind a field to the specified color resource ID. Type can be {@code int}.
 * <pre><code>
 * {@literal @}BindColor(ResourceTable.Color_background_green) int green;
 * {@literal @}BindColor(ResourceTable.Color_background_green_selector) ColorStateList greenSelector;
 * </code></pre>
 */
@Target(FIELD)
@Retention(RUNTIME)
public @interface BindColor {
    /**
     * Color resource ID to which the field will be bound.
     */
    int value();
}
