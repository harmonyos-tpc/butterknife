package butterknife;

import butterknife.internal.ListenerClass;
import butterknife.internal.ListenerMethod;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Bind a method to an { setClickedListener setClickedListener} on the component for each ID specified.
 * <pre><code>
 * {@literal @}OnClick(ResourceTable.Id_example) void onClick() {
 *   //onClick action
 * }
 * </code></pre>
 * Any number of parameters from
 * { setClickedListener#onClick(ohos.agp.components.Component) onClick} may be used on the
 * method.
 * <p>
 * setClickedListener
 */
@Target(METHOD)
@Retention(RUNTIME)
@ListenerClass(
        targetType = "ohos.agp.components.Component",
        setter = "setClickedListener",
        type = "butterknife.internal.DebouncingOnClickListener",
        method = @ListenerMethod(
                name = "doClick",
                parameters = "ohos.agp.components.Component"
        )
)
public @interface OnClick {
    /**
     * Component IDs to which the method will be bound.
     */
    int[] value() default { /* View.NO_ID */};
}
