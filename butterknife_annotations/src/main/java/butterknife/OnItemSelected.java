package butterknife;

import butterknife.internal.ListenerClass;
import butterknife.internal.ListenerMethod;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;


@Target(METHOD)
@Retention(RUNTIME)
@ListenerClass(
        targetType = "ohos.agp.components.ListContainer",
        setter = "setItemSelectedListener",
        type = "ohos.agp.components.ListContainer.ItemSelectedListener",
        method = @ListenerMethod(
                name = "onItemSelected",
                parameters = {
                        "ohos.agp.components.ListContainer",
                        "ohos.agp.components.Component",
                        "int",
                        "long"
                }
        )
)
public @interface OnItemSelected {
    int[] value() default { /* View.NO_ID */};
}


