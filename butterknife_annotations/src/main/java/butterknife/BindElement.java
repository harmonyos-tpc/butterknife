/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package butterknife;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static butterknife.internal.Constants.NO_RES_ID;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Bind a field to the specified element resource ID.
 * <pre><code>
 * {@literal @}BindElement(ResourceTable.Media_placeholder)
 * Element placeholder;
 * {@literal @}BindElement(value = ResourceTable.Media_placeholder, tint = ResourceTable.Id_colorAccent)
 * Element tintedPlaceholder;
 * </code></pre>
 */
@Target(FIELD)
@Retention(RUNTIME)
public @interface BindElement {
    /**
     * Element resource ID to which the field will be bound.
     */
    int value();

    /**
     * Color attribute resource ID that is used to tint the element.
     */
    int tint() default NO_RES_ID;
}
