package butterknife;

import butterknife.internal.ListenerClass;
import butterknife.internal.ListenerMethod;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Bind a method to an { FocusChangedListener FocusChangedListener} on the component for each ID
 * specified.
 * <pre><code>
 * {@literal @}OnFocusChange(ResourceTable.Id_example) void onFocusChange(Component component, boolean hasFocus) {
 *   // Focus changes action
 * }
 * </code></pre>
 * Any number of parameters from { FocusChangedListener#onFocusChange(ohos.agp.components.Component,
 * boolean) onFocusChange} may be used on the method.
 * <p>
 * FocusChangedListener
 */
@Target(METHOD)
@Retention(RUNTIME)
@ListenerClass(
        targetType = "ohos.agp.components.Component",
        setter = "setFocusChangedListener",
        type = "ohos.agp.components.Component.FocusChangedListener",
        method = @ListenerMethod(
                name = "onFocusChange",
                parameters = {
                        "ohos.agp.components.Component",
                        "boolean"
                }
        )
)
public @interface OnFocusChange {
    /**
     * Component IDs to which the method will be bound.
     */
    int[] value() default { /* View.NO_ID */};
}
