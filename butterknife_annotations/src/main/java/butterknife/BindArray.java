package butterknife;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Bind a field to the specified array resource ID. The type of array will be inferred from the
 * annotated element.
 * <p>
 * String array:
 * <pre><code>
 * {@literal @}BindArray(ResourceTable.Strarray_countries) String[] countries;
 * </code></pre>
 * <p>
 * Int array:
 * <pre><code>
 * {@literal @}BindArray(ResourceTable.Intarray_phones) int[] phones;
 * </code></pre>
 * <p>
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface BindArray {
    /**
     * Array resource ID to which the field will be bound.
     */
    int value();
}
