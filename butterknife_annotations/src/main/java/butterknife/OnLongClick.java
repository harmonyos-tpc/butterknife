package butterknife;

import butterknife.internal.ListenerClass;
import butterknife.internal.ListenerMethod;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Bind a method to an { LongClickedListener LongClickedListener} on the component for each ID
 * specified.
 * <pre><code>
 * {@literal @}OnLongClick(ResourceTable.Id_example) boolean onLongClicked(Component) {
 *   //Long click action
 *   return true;
 * }
 * </code></pre>
 * Any number of parameters from { LongClickedListener#onLongClicked(ohos.agp.components.Component)} may be
 * used on the method.
 * <p>
 * If the return type of the method is {@code void}, true will be returned from the listener.
 * <p>
 * LongClickedListener
 */
@Target(METHOD)
@Retention(RUNTIME)
@ListenerClass(
        targetType = "ohos.agp.components.Component",
        setter = "setLongClickedListener",
        type = "ohos.agp.components.Component.LongClickedListener",
        method = @ListenerMethod(
                name = "onLongClicked",
                parameters = {
                        "ohos.agp.components.Component"
                }
        )
)
public @interface OnLongClick {
    /**
     * View IDs to which the method will be bound.
     */
    int[] value() default { /* View.NO_ID */};
}
