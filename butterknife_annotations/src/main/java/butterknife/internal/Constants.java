package butterknife.internal;

/**
 * Constants values of ButterKnife
 */

public class Constants {
    private Constants() {
    }

    /**
     * Evaluates the script starting with {@value #NO_RES_ID}.
     */
    public static final int NO_RES_ID = -1;
}