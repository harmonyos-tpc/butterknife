package butterknife.internal;


import ohos.agp.components.Component;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

/**
 * A { Component.ClickedListener click listener} that debounces multiple clicks posted in the
 * same frame. A click on one button disables all buttons for that frame.
 */
public abstract class DebouncingOnClickListener implements Component.ClickedListener {
  private static final Runnable ENABLE_AGAIN = () -> enabled = true;
  private static final EventHandler MAIN = new EventHandler(EventRunner.getMainEventRunner());

  static boolean enabled = true;

  @Override public final void onClick(Component v) {
    if (enabled) {
      enabled = false;

      // Post to the main looper directly rather than going through the view.
      // Ensure that ENABLE_AGAIN will be executed, avoid static field {@link #enabled}
      // staying in false state.
      MAIN.postTask(ENABLE_AGAIN);

      doClick(v);
    }
  }

  public abstract void doClick(Component v);
}
