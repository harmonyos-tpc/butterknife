package butterknife;

import ohos.agp.components.Component;

/** An action that can be applied to a list of views. */
public interface Action<T extends Component> {
  /** Apply the action on the {@code view} which is at {@code index} in the list.
   * @param view component
   * @param index list index
   * */
  void apply(T view, int index);
}
