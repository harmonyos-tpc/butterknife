package butterknife;


import ohos.agp.components.Component;

/** A setter that can apply a value to a list of views. */
public interface Setter<T extends Component, V> {
  /** Set the {@code value} on the {@code view} which is at {@code index} in the list.
   * @param view on the component
   * @param value new value
   * @param index list index
   * */
  void set(T view, V value, int index);
}
