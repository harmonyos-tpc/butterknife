## ButterKnife

## Introduction
**Field and method binding for openharmony Components which uses annotation processing to generate boilerplate code for you.**

 * Eliminate `findComponentById` calls by using `@BindComponent` on fields.
 * Group multiple views in a list or array. Operate on all of them at once with actions,
   setters, or properties.
 * Eliminate anonymous inner-classes for listeners by annotating methods with `@OnClick` and others.
 * Eliminate resource lookups by using resource annotations on fields.


### Features

Added Support for

- *@BindArray*
- *@BindBool*
- *@BindColor*
- *@BindComponent*
- *@BindComponents*
- *@BindDimen*
- *@BindElement*
- *@BindFloat*
- *@BindFont*
- *@BindInt*
- *@BindPixelMap*
- *@BindString*
- *@OnCheckedChanged*
- *@OnClick*
- *@OnEditorAction*
- *@OnFocusChange*
- *@OnItemClick*
- *@OnItemLongClick*
- *@OnItemSelected*
- *@OnLongClick*
- *@OnPageChange*
- *@OnTextChanged*
- *@OnTouch*
- *@OnOptional*
 
## Usage Instructions

```java
class ExampleAbility extends Ability {
  @BindComponent(ResourceTable.Id_user) TextField username;
  @BindComponent(ResourceTable.Id_pass) TextField password;

  @BindString(ResourceTable.String_login_error) String loginErrorMessage;

  @OnClick(ResourceTable.Id_submit) void submit() {
    // TODO call server...
  }

  @Override 
  public void onStart(Intent intent) {
    super.onStart(intent);
    ComponentContainer rootlayout = (ComponentContainer) LayoutScatter.getInstance(this).parse(ResourceTable.Layout_ability_main, null, false);
    ButterKnife.bind(this, rootlayout);
    // TODO Use fields...
  }
}
```

## Installation Instructions
```
Method 1: Generate har package from library and add it to lib folder.
       add following code to gradle of entry
    
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    annotationProcessor files('libs/butterknife_compiler.jar')

Method 2: 
    allprojects{ 
        repositories{ 
            mavenCentral() 
        } 
    }
    implementation 'io.openharmony.tpc.thirdlib:butterKnife:1.0.2'
    annotationProcessor 'io.openharmony.tpc.thirdlib:butterknife-compiler:1.0.2'
```

## License

    Copyright 2013 Jake Wharton

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
	