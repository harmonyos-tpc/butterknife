package butterknife.compiler;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;

import javax.annotation.Nullable;

final class FieldTypefaceBinding implements ResourceBinding {
    private static final ClassName TYPEFACE = ClassName.get("ohos.agp.text", "Font");

    enum TypefaceStyles {
        REGULAR(0),
        BOLD(1),
        ITALIC(2),
        BOLD_ITALIC(3);

        final int value;

        TypefaceStyles(int value) {
            this.value = value;
        }

        @Nullable
        static TypefaceStyles fromValue(int value) {
            for (TypefaceStyles style : values()) {
                if (style.value == value) {
                    return style;
                }
            }
            return null;
        }
    }

    private final Id id;
    private final String name;
    private final TypefaceStyles style;
    private final Boolean italic;

    FieldTypefaceBinding(Id id, String name, TypefaceStyles style) {
        TypefaceStyles style1;
        this.id = id;
        this.name = name;
        style1 = style;
        if (style.value == 2 || style.value == 3)
            this.italic = true;
        else
            this.italic = false;
        if (style.value == 2)
            style1 = TypefaceStyles.REGULAR;
        else if (style.value == 3)
            style1 = TypefaceStyles.BOLD;
        this.style = style1;
    }

    @Override
    public Id id() {
        return id;
    }

    @Override
    public boolean requiresResources(int sdk) {
        return sdk >= 26;
    }

    @Override
    public CodeBlock render(int sdk) {
        CodeBlock typeface = CodeBlock.of("Utils.getFont(context, $1L, $2T.$3L, $4L)", id.code, TYPEFACE, style, italic);
//        if (style != TypefaceStyles.REGULAR) {
//            typeface = CodeBlock.of("$1T.Builder($2L, $1T.$3L)", TYPEFACE, typeface, style);
//        }
        return CodeBlock.of("target.$L = $L", name, typeface);
    }
}
