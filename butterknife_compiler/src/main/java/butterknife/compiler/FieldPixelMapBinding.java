/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package butterknife.compiler;

import com.squareup.javapoet.CodeBlock;

import static butterknife.compiler.BindingSet.RES_UTILS;

final class FieldPixelMapBinding implements ResourceBinding {
    private final Id id;
    private final String name;

    FieldPixelMapBinding(Id id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public Id id() {
        return id;
    }

    @Override
    public boolean requiresResources(int sdk) {
        return false;
    }

    @Override
    public CodeBlock render(int sdk) {
        return CodeBlock.of("target.$L = $T.getPixelMapVal(context, $L)", name, RES_UTILS, id.code);
    }
}
