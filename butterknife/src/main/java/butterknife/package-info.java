/**
 * Field and method binding for openharmony components which uses annotation processing to generate
 * boilerplate code for you.
 * <p>
 * <ul>
 * <li>Eliminate { ohos.agp.components.Component#findComponentById findComponentById} calls by using
 * { butterknife.BindComponent @BindComponent} on fields.</li>
 * <li>Group multiple views in a {@linkplain java.util.List list} or array.
 * <li>Eliminate anonymous inner-classes for listeners by annotating methods with
 * { butterknife.OnClick @OnClick} and others.</li>
 * <li>Eliminate resource lookups by using resource annotations on fields.</li>
 * </ul>
 */
package butterknife;
