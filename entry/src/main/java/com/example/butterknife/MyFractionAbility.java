package com.example.butterknife;

import butterknife.BindComponent;
import butterknife.ButterKnife;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;

public class MyFractionAbility extends FractionAbility {

    @BindComponent(ResourceTable.Id_text)
    Text text;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_my_fraction);
        ButterKnife.bind(this);

        text.setText("鸿蒙，你好");
    }

}
