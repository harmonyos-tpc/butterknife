/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.butterknife.view;

import com.example.butterknife.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;

public class SamplePageSliderView extends AbstractPageView {

    public SamplePageSliderView(AbilitySlice abilitySlice, String name, Color txtColor) {
        super(abilitySlice, name, txtColor);

    }

    public SamplePageSliderView(AbilitySlice abilitySlice, String name, int iconId, int iconIdSelected, Color txtColor) {
        super(abilitySlice, name, iconId, iconIdSelected, txtColor);
    }

    @Override
    public void initView() {
        super.setRootView(loadView());
    }

    private Component loadView(){
        ComponentContainer layout = (ComponentContainer) LayoutScatter.getInstance(super.getSlice()).
                parse(com.example.butterknife.ResourceTable.Layout_slice_sample, null, false);
        Text mTxtStatus = (Text) layout.findComponentById(ResourceTable.Id_txtStatus);
        mTxtStatus.setTextColor(getTxtColor());
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 16; i++) {
            builder.append(super.getName()).append(" ");
        }
        builder.deleteCharAt(builder.length() - 1);
        mTxtStatus.setText(builder.toString());
        return layout;
    }
}
