/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.butterknife.view;

import butterknife.internal.util.LogUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;

public abstract class AbstractPageView implements PageInfo {
    private static final String TAG = AbstractPageView.class.getCanonicalName();
    private String name;
    private int iconId = 0;
    private int iconIdSelected = 0;
    private Component rootView;
    private Color txtColor;
    protected AbilitySlice slice;

    public abstract void initView();

    public AbstractPageView(AbilitySlice abilitySlice, String name, Color txtColor) {
        if (abilitySlice != null) {
            this.slice = abilitySlice;
        } else {
            LogUtil.error(TAG, "slice is null, set default slice");
            this.slice = new AbilitySlice();
        }
        this.name = name;
        this.txtColor = txtColor;
        initView();
    }

    public AbstractPageView(AbilitySlice abilitySlice, String str, int iconId, int iconIdSelected, Color txtColor) {
        if (abilitySlice != null) {
            this.slice = abilitySlice;
        } else {
            LogUtil.error(TAG, "slice is null, set default slice");
            this.slice = new AbilitySlice();
        }
        this.name = str;
        this.iconId = iconId;
        this.iconIdSelected = iconIdSelected;
        this.txtColor = txtColor;
        initView();
    }


    public AbilitySlice getSlice() {
        return this.slice;
    }

    public String getName() {
        return this.name;
    }

    public int getIconId() {
        return this.iconId;
    }

    public int getIconIdSelected() {
        return this.iconIdSelected;
    }

    public void setIconIdSelected(int i) {
        this.iconIdSelected = i;
    }

    public void setIconId(int i) {
        this.iconId = i;
    }

    public Color getTxtColor(){
        return this.txtColor;
    }

    public void setTxtColor(Color txtColor) {
        this.txtColor = txtColor;
    }

    public Component getRootView() {
        return this.rootView;
    }

    public void setRootView(Component component) {
        this.rootView = component;
    }

}