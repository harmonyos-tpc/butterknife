/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.butterknife.slice;

import butterknife.*;
import butterknife.internal.util.ResUtil;
import com.example.butterknife.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;

public class NewTestCaseSlice extends AbilitySlice {
    @BindComponent(ResourceTable.Id_radiobtn1)
    RadioButton radioBtn1;
    @BindComponent(ResourceTable.Id_radiobtn2)
    RadioButton radioBtn2;

    @BindComponent(ResourceTable.Id_image)
    Image image;

    //Testcase no-21
    @BindElement(ResourceTable.Media_icon)
    Element imageElement;

    @BindComponent(ResourceTable.Id_viewRoot)
    DirectionalLayout mViewRoot;

    @BindColor(ResourceTable.Color_Green)
    int Color_Green;

    //Testcase no-22
    @OnCheckedChanged(ResourceTable.Id_radioCont)
    void onChecked​() {
        ResUtil.showToast(getContext(), "I'm App RadioContainer", 2000);
    }

    //Testcase no-23
    @OnTextChanged(ResourceTable.Id_txtFeild)
    void textchanged(){
        ResUtil.showToast(getContext(), "Text Changed", 2000);

    }



    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_slice_new_test_case);
        ButterKnife.bind(this);

        image.setImageElement(imageElement);


        radioBtn1.setBackground(ResUtil.buildDrawableByColor(Color_Green));
        radioBtn2.setBackground(ResUtil.buildDrawableByColor(Color_Green));



    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

}
