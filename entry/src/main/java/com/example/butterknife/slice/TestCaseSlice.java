/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.butterknife.slice;

import butterknife.*;
import butterknife.internal.util.ResUtil;
import com.example.butterknife.MyFractionAbility;
import com.example.butterknife.ResourceTable;
import com.example.butterknife.view.AbstractPageView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;

import java.util.List;

public class TestCaseSlice extends AbstractPageView {
    ComponentContainer mRootLayout;

    @OnClick(ResourceTable.Id_next)
    void nextSlice(){
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ABILITY_NEW_MISSION);
        intent.setParam("testcase", 1);
        getSlice().present(new NewTestCaseSlice(), intent);
    }

    @OnClick(ResourceTable.Id_go2fraction)
    void go2fraction(){
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(getSlice().getBundleName())
                .withAbilityName(MyFractionAbility.class)
                .build();
        intent.setOperation(operation);
        getSlice().startAbility(intent);
    }

    // Case 1: @BindComponent
    @BindComponent(ResourceTable.Id_viewRoot)
    DirectionalLayout mViewRoot;
    @BindComponent(ResourceTable.Id_list_of_things)
    ListContainer mListOfThings;
    @BindComponent(ResourceTable.Id_txtInput)
    TextField mTxtInput;

    // Case 2 : @BindColor
    @BindColor(ResourceTable.Color_hybrid_grey)
    int mHybridGrey;
    @BindColor(ResourceTable.Color_white)
    int mColorWhite;
    @BindColor(ResourceTable.Color_silver_grey)
    int mColorSilver;

    // Case 3: @BindComponents
    @BindComponents({ResourceTable.Id_txtTitle, ResourceTable.Id_txtSubTitle, ResourceTable.Id_btnHello})
    List<Component> headerViews;

    // Case 4: @BindString
    @BindString(ResourceTable.String_app_title)
    String mAppTitle;
    @BindString(ResourceTable.String_app_content)
    String mAppContent;
    @BindString(ResourceTable.String_say_hello)
    String mSayHello;

    // Case 5: @BindInt
    @BindInt(ResourceTable.Integer_default_position)
    int mDefaultPosition;

    // Case 6: @BindBool
    @BindBool(ResourceTable.Boolean_default_btn_visibility)
    boolean mIsBtnVisible;

    // Case 7: @BindDimen
    @BindDimen(ResourceTable.Float_default_btn_txt_size)
    int mIntBtnSize;

    // Case 8: @BindFloat
    @BindFloat(ResourceTable.Float_default_btn_txt_size)
    float mFloatBtnSize;

    // Case 9: @BindArray
    @BindArray(ResourceTable.Strarray_Components)
    String[] mComponentArray;

    // Case 10: @OnClick
    @OnClick(ResourceTable.Id_btnHello)
    void sayHello() {
        ResUtil.showToast(slice.getContext(), "Hello Components", 2000);
    }

    // Case 11: @OnLongClick
    @OnLongClick(ResourceTable.Id_btnHello) void sayGetOffMe() {
        ResUtil.showToast(slice.getContext(), "OnTxtLongClick ", 2000);
    }

    // Case 12: @OnTouch & Case 13: @Optional
    @Optional
    @OnTouch(ResourceTable.Id_txtTitle) boolean touchedTitle() {
        ResUtil.showToast(slice.getContext(), "I'm App Title and Default Position is : "
                + mDefaultPosition, 2000);
        return false;
    }

    // Case 14: @OnItemClick
    @OnItemClick(ResourceTable.Id_list_of_things) void onItemClick(int position) {
        ResUtil.showToast(slice.getContext(), "You clicked Item : " + position, 2000);
    }

    // Case 15: @OnItemLongClick
    @OnItemLongClick(ResourceTable.Id_list_of_things) boolean onItemLongClick(int position) {
        ResUtil.showToast(slice.getContext(), "You Long clicked Item : " + position, 2000);
        return true;
    }

    // Case 16: @OnItemSelected
    @OnItemSelected(ResourceTable.Id_list_of_things) void onItemSelect(int position) {
        ResUtil.showToast(slice.getContext(), "You Selected Item : " + position, 2000);
    }

    // Case 17: @OnFocusChange
    @OnFocusChange(ResourceTable.Id_txtInput)void onFocusChanged(Component component, boolean hasFocus) {
        ResUtil.showToast(slice.getContext(), "Component Focus : " + hasFocus, 2000);
    }

    // Case 18: @OnEditorAction
    @OnEditorAction(ResourceTable.Id_txtInput) boolean onTextEditorAction(int action) {
        ResUtil.showToast(slice.getContext(), "Text Editor Action : " + action, 2000);
        return true;
    }

    // Case 19: @BindFont
    @BindFont(value = ResourceTable.String_aargh_font, style = 2)
    Font mAaarghFont;

    public TestCaseSlice(AbilitySlice abilitySlice, String name, Color txtColor) {
        super(abilitySlice, name, txtColor);
    }


    @Override
    public void initView() {
        super.setRootView(loadView());
    }

    private Component loadView() {
        mRootLayout = (ComponentContainer) LayoutScatter.getInstance(super.getSlice()).
                parse(ResourceTable.Layout_slice_test, null, false);
        ButterKnife.bind(this, mRootLayout);
        ((Text) headerViews.get(0)).setText(mAppTitle);
        ((Text) headerViews.get(1)).setText(mAppContent);
        Button btnHello = (Button) headerViews.get(2);
        btnHello.setText(mSayHello);
        btnHello.setFont(Font.MONOSPACE);
        btnHello.setTextSize(mIntBtnSize);
        btnHello.setBackground(ResUtil.buildDrawableByColor
                (mHybridGrey));
        if (mIsBtnVisible) {
            btnHello.setVisibility(Component.VISIBLE);
        }else {
            btnHello.setVisibility(Component.INVISIBLE);
        }

        mTxtInput.setBackground(ResUtil.buildDrawableByColor
                (mColorWhite));
        mTxtInput.setTextColor(Color.WHITE);
        mTxtInput.setFont(mAaarghFont);
        mListOfThings.setBackground(ResUtil.buildDrawableByColor
                (mColorSilver));
        mListOfThings.setItemProvider(new MainListProvider(mComponentArray));
        return mRootLayout;
    }

    public class MainListProvider extends BaseItemProvider {
        String[] list_items;
        @BindComponent(ResourceTable.Id_txtWord) Text word;

        MainListProvider(String[] lst) {
            list_items = lst;
        }

        @Override
        public int getCount() {
            return list_items.length;
        }

        @Override
        public Object getItem(int i) {
            return list_items[i];
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
            if (component == null) {
                component = LayoutScatter.getInstance(slice.getContext()).parse(ResourceTable.Layout_list_item, componentContainer, false);
            }
            ButterKnife.bind(this, component);
            word.setBackground(ResUtil.buildDrawableByColor
                    (ResUtil.getColor(slice.getContext(), ResourceTable.Color_hybrid_grey)));
            word.setText((String) getItem(i));
            return component;
        }
    }
}
