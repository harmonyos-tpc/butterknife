/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.butterknife.slice;

import butterknife.BindComponent;
import butterknife.OnPageChange;
import butterknife.ButterKnife;
import butterknife.internal.util.ResUtil;
import com.example.butterknife.ResourceTable;
import com.example.butterknife.view.AbstractPageView;
import com.example.butterknife.view.PageViewAdapter;
import com.example.butterknife.view.SamplePageSliderView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;

import java.util.ArrayList;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {

    ComponentContainer mRootLayout;

    @BindComponent(ResourceTable.Id_viewRoot)
    DirectionalLayout mDlViewRoot;
    @BindComponent(ResourceTable.Id_slider)
    PageSlider mPager;
    @BindComponent(ResourceTable.Id_indicator)
    PageSliderIndicator mIndicator;

    // Case 20: @OnPageChange
    @OnPageChange(ResourceTable.Id_slider) void onPageSlideStateChanged(int position) {
        ResUtil.showToast(getContext(), "Selected Page : " + position, 2000);
    }

    private List<AbstractPageView> mPageViews;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_slice_main);
        ButterKnife.bind(this);
        mDlViewRoot.setBackground(ResUtil.buildDrawableByColor((Color.LTGRAY).getValue()));
        initPageView(intent);
    }

    private void initPageView(Intent intent) {
        mPageViews = new ArrayList();
        mPageViews.add(new TestCaseSlice(this, "Sample Page", Color.BLACK));
        mPageViews.add(new SamplePageSliderView(this, "Sample Page", Color.BLACK));
        PageViewAdapter pageViewAdapter = new PageViewAdapter(this, mPageViews);
        mPager.setProvider(pageViewAdapter);
        mPager.setOrientation(Component.HORIZONTAL);
        mPager.setSlidingPossible(true);
        mIndicator.setPageSlider(mPager);
        mIndicator.setItemOffset(7);
        mIndicator.setItemElement(
                ResUtil.getCustomRectGradientDrawable(new Color(0xFF888888), new Rect(0, 0, 70, 12)),
                ResUtil.getCustomRectGradientDrawable(new Color(0x88FF0000), new Rect(0, 0, 70, 12)));
    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }


}
